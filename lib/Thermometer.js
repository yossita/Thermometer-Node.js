var stream = require('stream');
var util = require('util');

//factory
function Thermometer() {
    if (!(this instanceof Thermometer)) {
        return new Thermometer();
    }

    stream.Transform.apply(this);
    this._writableState.objectMode = true;
    this._readableState.objectMode = true;
}

util.inherits(Thermometer, stream.Transform);

Thermometer.prototype._transform = function (chunk, enc, callback) {
    callback(null, chunk);
};

//export factory
module.exports = Thermometer;