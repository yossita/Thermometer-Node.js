var fs = require('fs');
var path = require('path');

module.exports = function(config_path){
    config_path = path.resolve(config_path); 
    if (!fs.existsSync(config_path)){
        throw Error(config_path + ' not found');
    }
    var config = JSON.parse(fs.readFileSync(config_path, { "flag":"r"}));

    return config;
};