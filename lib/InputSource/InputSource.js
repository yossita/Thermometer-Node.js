var through = require('through2');

module.exports = function () {
    var _started = false;
    var _strategy;

    var source = through.obj(function (chunk, enc, cb) {
        if (!_started) {
            return cb(null, null);
        }
        return cb(null, chunk);
    });

    source.use = function (strategy) {
        _strategy = strategy;
    };

    source.start = function () {
        if (!_strategy) {
            throw Error("No strategy defined for input source");
        }
        _started = true;
        _strategy.pipe(this);
    };

    source.stop = function () {
        _started = false;
        _strategy.unpipe(this);
        source.push(null);
    };

    return source;
};
