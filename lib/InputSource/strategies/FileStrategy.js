var fs = require('fs');
var split = require('split');
var through = require('through2');
var Celsius = require('../../Temperature').Celsius;

module.exports = function (file) {
    return fs.createReadStream(file)
        .pipe(split())
        .pipe(through.obj(function (line, enc, cb) {
            cb(null, new Celsius(line));
        }));
};