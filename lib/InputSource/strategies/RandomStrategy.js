var from = require("from2");
var Celsius = require("../../Temperature").Celsius;

module.exports = function (options) {

    var _last_random_reading;
    var timeout = (options && options.timeout_between_readings) || 1000;

    function nextRandom() {
        var sign = Number.parseInt((Math.random() * 10)) % 2 == 0 ? 1 : -1;
        var value = Number.parseFloat((Math.random() * 10 * sign).toPrecision(2));
        if (!_last_random_reading) {
            _last_random_reading = new Celsius(value);
        }
        else {
            _last_random_reading = _last_random_reading.add(value);
        }
        return _last_random_reading;
    }

    return from.obj(function (size, next) {
        setTimeout(function () {
            nextRandom(_last_random_reading);
            next(null, _last_random_reading);
        }, timeout);
    });
};