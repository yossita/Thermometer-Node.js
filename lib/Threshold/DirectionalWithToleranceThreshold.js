var Threshold = require("./Threshold").Threshold;
var util = require('util');

//ctor
function DirectionalWithToleranceThreshold(threshold, direction, fluctuation) {
    fluctuation = fluctuation || 0;
    if (!(this instanceof DirectionalWithToleranceThreshold)){
        return new DirectionalWithToleranceThreshold(threshold, direction, fluctuation);
    }

    Threshold.apply(this);
    this._upper_threshold = threshold.add(fluctuation);
    this._lower_threshold = threshold.add(-fluctuation);
    this._threshold = threshold;
    this._direction = direction;

    this._last_reading = undefined;
    this._armed = false;
}

util.inherits(DirectionalWithToleranceThreshold, Threshold);

DirectionalWithToleranceThreshold.prototype._evaluate = function (reading) {

    function _isTrendingDown(first, second) {
        return first.gt(second);
    }

    function _isTrendingUp(first, second) {
        return first.lt(second);
    }

    function _isInsideBoundries(reading, upper, lower) {
        return reading.le(upper) && reading.ge(lower);
    }

    function _crossedThreshold(first, second, threshold) {
        return (first.lt(threshold) && second.ge(threshold)) || (first.gt(threshold) && second.le(threshold));
    }

    var last = this._last_reading;
    var upper = this._upper_threshold;
    var lower = this._lower_threshold;
    var threshold = this._threshold;
    var armed = this._armed;

    var crossed = false;

    if (armed) {
        armed = _isInsideBoundries(reading, upper, lower);  //check fluctuation, disarm if outside fluctuation boundaries
    } else {
        if (last) {
            crossed = _crossedThreshold(last, reading, threshold);
            if (this._direction.toLowerCase() == 'up') {
                crossed = crossed && _isTrendingUp(last, reading);
            }
            else if (this._direction.toLowerCase() == 'down') {
                crossed = crossed && _isTrendingDown(last, reading);
            }
        } else {
            crossed = threshold.eq(reading);
        }
        armed = crossed;    //arm if crossed
    }

    //save state
    this._last_reading = reading;
    this._armed = armed;
    return crossed;
};

module.exports = DirectionalWithToleranceThreshold;