var Threshold = require("./Threshold").Threshold;
var util = require('util');

function ExactTemperatureThreshold(threshold) {
    if (!(this instanceof ExactTemperatureThreshold)){
        return new ExactTemperatureThreshold(threshold);
    }
    Threshold.apply(this);
    this._threshold = threshold;
}

ExactTemperatureThreshold.prototype._evaluate = function (reading) {
    return this._threshold.eq(reading);
};

util.inherits(ExactTemperatureThreshold, Threshold);

module.exports = ExactTemperatureThreshold;