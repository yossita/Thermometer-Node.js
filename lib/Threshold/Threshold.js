var through = require('through2');

/* base object for thresholds */
function Threshold() { }

/* template method */
Threshold.prototype._evaluate = function (reading) {
    throw new Error("not implemented");
};

// Threshold.prototype.reached = function (current_reading) {
//     return this._evaluate(current_reading);
// };

module.exports = function (options) {
    var threshold_value = options.threshold;
    var direction = options.direction;
    var fluctuation = options.fluctuation || 0.0;
    var threshold;
    if (direction) {
        threshold = require("./DirectionalWithToleranceThreshold")(threshold_value, direction, fluctuation);
    } else {
        threshold = require("./ExactTemperatureThreshold")(threshold_value);
    }
    return through({ "objectMode": true }, function (chunk, enc, callback) {
        if (threshold._evaluate(chunk)) {
            callback(null, chunk);
        } else {
            callback();
        }
    });
};

module.exports.Threshold = Threshold;