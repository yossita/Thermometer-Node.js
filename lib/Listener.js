var stream = require('stream');
var util = require('util');

var listener = function (callback) {
    if (!(this instanceof listener)) {
        return new listener(callback);
    }
    stream.Writable.call(this, { 'objectMode': true });
    this._callback = callback;
};

util.inherits(listener, stream.Writable);

listener.prototype._write = function (chunk, encoding, callback) {
    this._callback(chunk);
    callback();
};

module.exports = listener;