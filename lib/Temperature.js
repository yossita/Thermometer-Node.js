
//factory
function create(type, value) {
    switch (type.toLowerCase()) {
        case Celsius.name.toLowerCase():
            return new Celsius(value);
        case Fahrenheit.name.toLowerCase():
            return new Fahrenheit(value);
        default:
            throw new Error(type, ' is not supported');
    }
}

function Temperature(value) {
    this._value = typeof value == 'string' ? Number.parseFloat(value) : value;
}

Temperature.prototype.value = function () {
    return this._value;
};

Temperature.prototype._convertToCelsius = function () {
    throw new Error("not implemented");
};

Temperature.prototype.eq = function (other) {
    return this._convertToCelsius()._value == other._convertToCelsius()._value;
};

Temperature.prototype.gt = function (other) {
    return this._convertToCelsius()._value > other._convertToCelsius()._value;
};

Temperature.prototype.lt = function (other) {
    return this._convertToCelsius()._value < other._convertToCelsius()._value;
};

Temperature.prototype.ge = function (other) {
    return this._convertToCelsius()._value >= other._convertToCelsius()._value;
};

Temperature.prototype.le = function (other) {
    return this._convertToCelsius()._value <= other._convertToCelsius()._value;
};

Temperature.prototype.add = function (value) {
    return create(this.name, this._value + value);
};

function Celsius(value) {
    Temperature.apply(this, arguments);
    this.name = 'celsius';
}

Celsius.prototype = new Temperature();
Celsius.constructor = Celsius;

Celsius.prototype._convertToCelsius = function () {
    return this;
};

function Fahrenheit(value) {
    Temperature.apply(this, arguments);
    this.name = "fahrenheit";
}

Fahrenheit.prototype = new Temperature();
Fahrenheit.constructor = Fahrenheit;

Fahrenheit.prototype._convertToCelsius = function () {
    return new Celsius((this._value - 32) / 1.8);
};

//export factories
module.exports.Celsius = function (value) {
    return create(Celsius.name, value);
};
module.exports.Fahrenheit = function (value) {
    return create(Fahrenheit.name, value);
};