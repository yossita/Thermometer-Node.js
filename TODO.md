**_Code to do:_**
1. ~~Generalized modules~~
1. ~~Unit tests~~
1. ~~Convert to node.js patterns~~
1. Connect source to weather provider
1. ~~Create http server~~
1. Convert to es2015
1. convert to es2016

**_UI to do_**
1. Create SPA to consume temperature
1. Deploy to heroku
1. Add new threshold
1. List thresholds
1. Remove threshold