var InputSource = require("./lib/InputSource/InputSource");
var RandomStrategy = require("./lib/InputSource/Strategies/RandomStrategy");
var Thermometer = require("./lib/Thermometer");
var Celsius = require("./lib/Temperature").Celsius;
var Fahernheit = require("./lib/Temperature").Fahrenheit;
var Listener = require("./lib/Listener");
var Threshold = require("./lib/Threshold/Threshold");
var config = require("./lib/config");

var thresholds = [
    Threshold({ "threshold": new Fahernheit(32) }),
    Threshold({ "threshold": Celsius(3), "direction": 'up', "fluctuation": 0.5 }),
    Threshold({ "threshold": Celsius(-1.5), "direction": 'down', "fluctuation": 0.1 }),
    Threshold({ "threshold": Celsius(10), "direction": 'any', "fluctuation": 1.0 })
];

var listeners = [
    Listener(function (reading) {
        console.log("reached freezing point");
    }),
    Listener(function (reading) {
        console.log('reached 3C+-0.5 from below');
    }),
    Listener(function (reading) {
        console.log('reached -1.5C+-0.1 from above');
    }),
    Listener(function (reading) {
        console.log('reached 10C+-1');
    })
];

var generic_listener = Listener(function (reading) {
    console.log('read ', reading);
});

var source = InputSource();
source.use(RandomStrategy());
var thermometer = Thermometer();
source
    .pipe(thermometer)
    .pipe(generic_listener);
source.start();

for (var i = 0; i < listeners.length; i++) {
    var listener = listeners[i];
    var threshold = thresholds[i];
    thermometer
        .pipe(threshold)
        .pipe(listener);
}

var http = require('http');

function extract_temp(weather_data) {
    var data = JSON.parse(weather_data);
    return JSON.stringify({
        'currentTemperature': data.main.temp
    });
}

var port = 5000;
var ip = '127.0.0.1';
var conf = config(__dirname + "/config.json");

var server = http.createServer(function (req, res) {
    http.get({
        'hostname': conf['openweathermap-url'],
        'port': 80,
        'path': '/data/2.5/weather?q=Vancouver,BC&units=metric&APPID=' + conf['openweathermap-key'],
    }, function (get_res) {
        var weather_data = '';
        get_res
            .on('readable', function () {
                var buf;
                while ((buf = get_res.read())) {
                    weather_data += buf;
                }
            })
            .on('end', function () {
                res.end(extract_temp(weather_data));
            })
            .on('error', function (err) {
                console.log('error: ', err);
                res.statusCode = 500;
                res.end(err);
            });
    });

}).listen(port, ip);

console.log("Server listening on ", ip, ":", port);