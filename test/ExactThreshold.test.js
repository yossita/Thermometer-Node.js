var chai = require('chai');
chai.use(require('chai-events'));
var should = chai.should();

var Threshold = require('../lib/Threshold/Threshold');
var Celsius = require("../lib/Temperature").Celsius;


describe('Exact threshold', function () {
    var exact_value, threshold;
    beforeEach(function () {
        exact_value = Celsius(10.9);
        threshold = Threshold({ "threshold": exact_value });
    });

    describe('when writing same value', function () {
        var reading = Celsius(10.9);
        it('invoke data event', function () {
            var p = threshold.should.emit('data', exact_value);
            threshold.write(reading);
            return p;
        });

    });

    describe('when writing different value', function () {
        var reading = Celsius(10.9);
        it('doesnt invoke data event', function () {
            var p = threshold.should.not.emit('data');
            threshold.write(reading);
            return p;
        });

    });
});
