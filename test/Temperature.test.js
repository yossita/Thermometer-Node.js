var expect = require('chai').expect;

var Celsius = require("../lib/Temperature").Celsius;
var Fahernheit = require("../lib/Temperature").Fahrenheit;

describe('Temperature', function () {

    describe('factory', function () {

        it('should create celsius with value', function () {
            var celsius = new Celsius(5.5);

            expect(celsius).have.property("name", "celsius");
            expect(celsius.value()).to.equal(5.5);
        });

        it('should create fahrenheit with value', function () {
            var fahrenheit = new Fahernheit(5.5);

            expect(fahrenheit).have.property("name", "fahrenheit");
            expect(fahrenheit.value()).to.equal(5.5);
        });
      
        it('should compare equal C to F', function () {
            var c = new Celsius(0);
            var f = new Fahernheit(32);
            
            expect(c.eq(f)).to.be.true;
        });

        it('should compare equal C to C', function () {
            var c1 = new Celsius(32.4);
            var c2 = new Celsius(32.4);
            
            expect(c1.eq(c2)).to.be.true;
        });

        it('should compare equal F to F', function () {
            var f1 = new Fahernheit(-44.55);
            var f2 = new Fahernheit(-44.55);
            
            expect(f1.eq(f2)).to.be.true;
        });
        
    });

});
