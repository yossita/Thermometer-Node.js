var expect = require("chai").expect;

var InputSource = require("../lib/InputSource/InputSource");
var PredefinedValuesStrategy = require("../lib/InputSource/Strategies/PredefinedValuesStrategy");
var Thermometer = require("../lib/Thermometer");
var Listener = require("../lib/Listener");
var Celsius = require("../lib/Temperature").Celsius;
var Fahernheit = require("../lib/Temperature").Fahrenheit;
var Threshold = require('../lib/Threshold/Threshold');

describe("Thermometer", function () {
    describe("Configured", function () {
        it("Should call listeners", function (done) {

            var calls_collector = {};
            var processed_called_counter = 0;

            var threshold_call_collector = function (name) {
                return Listener(function (reading) {
                    console.debug(reading, name);
                    calls_collector[name] = (calls_collector[name] || 0) + 1;
                });
            };

            var freezing_threshold = Threshold({
                "threshold": new Fahernheit(32)
            });

            var three_up_threshold = Threshold({
                "threshold": new Celsius(3), "direction": "up", "fluctuation": 0.5
            });
            var minus_one_half_down_threshold = Threshold({
                "threshold": new Celsius(-1.5), "direction": "down", "fluctuation": 0.1
            });
            var ten_any_threshold = Threshold({
                "threshold": new Celsius(10), "direction": "any", "fluctuation": 1.0
            });

            freezing_threshold.pipe(threshold_call_collector('freezing'));
            three_up_threshold.pipe(threshold_call_collector('three'));
            minus_one_half_down_threshold.pipe(threshold_call_collector('onehalf'));
            ten_any_threshold.pipe(threshold_call_collector('ten'));

            var temperatures = [
                new Celsius(-5.5),
                new Celsius(-4.5),
                new Celsius(0),
                new Celsius(-1.3),
                new Celsius(-1.4),
                new Celsius(-1.7),
                new Celsius(3),
                new Celsius(3.1),
                new Celsius(3.2),
                new Celsius(2.4),
                new Celsius(9),
                new Celsius(10.1),
                new Celsius(8.9),
                new Celsius(10.1),
                new Celsius(9.9),
                new Fahernheit(50),
                new Fahernheit(60),
                new Fahernheit(34)
            ];
            var source = InputSource();
            source.use(PredefinedValuesStrategy(temperatures));

            var thermometer = Thermometer();

            source
                .on('end', function () {
                    expect(processed_called_counter).to.be.equal(temperatures.length);
                    expect(calls_collector).to.have.property('freezing').to.equal(1);
                    expect(calls_collector).to.have.property('onehalf').to.equal(1);
                    expect(calls_collector).to.have.property('three').to.equal(2);
                    expect(calls_collector).to.have.property('ten').to.equal(3);
                    done();
                }).on('data', function (data) {
                    processed_called_counter++;
                })
                .pipe(thermometer);

            thermometer.pipe(freezing_threshold);
            thermometer.pipe(three_up_threshold);
            thermometer.pipe(minus_one_half_down_threshold);
            thermometer.pipe(ten_any_threshold);

            source.start();
        });
    });
});