var expect = require("chai").expect;
var fs = require('fs');

var InputSource = require("../lib/InputSource/InputSource");
var Celsius = require("../lib/Temperature").Celsius;
var Fahrenheit = require("../lib/Temperature").Fahrenheit;

describe('InputSource', function () {

    var input_source;

    beforeEach(function () {
        input_source = InputSource();
    });

    describe('Using PredefinedValuesStrategyy', function () {

        var strategy = require('../lib/InputSource/Strategies/PredefinedValuesStrategy');

        it('Should trigger read events', function (done) {
            var temperature_list = [
                new Celsius(0),
                new Celsius(1),
                new Fahrenheit(35),
                new Fahrenheit(40),
                new Celsius(10)
            ];

            var idx = 0;

            input_source.use(strategy(temperature_list));

            input_source
                .on('readable', function () {
                    var reading;
                    while ((reading = this.read()) !== null) {
                        expect(reading).to.be.equal(temperature_list[idx++]);
                    }
                })
                .on('end', function () {
                    expect(idx).to.be.equal(temperature_list.length);
                    done();
                })
                .start();
        });
    });

    describe('Using FileStrategy', function () {

        var strategy = require('../lib/InputSource/Strategies/FileStrategy');

        it('Should trigger read events', function (done) {
            var file = __dirname + '/test_data.txt';

            var values = [];
            values = fs
                .readFileSync(file, { 'encoding': 'UTF8' })
                .split('\n')
                .map(function (value) { return Number.parseFloat(value); });
            var idx = 0;

            input_source.use(strategy(file));
            input_source
                .on('readable', function () {
                    var reading;
                    while ((reading = this.read()) !== null) {
                        expect(reading.value()).to.be.equal(values[idx++]);
                    }
                })
                .on('end', function () {
                    expect(idx).to.be.equal(values.length);
                    done();
                })
                .start();
        });

    });

    describe('Using RandomStrategy', function () {
        var strategy = require('../lib/InputSource/Strategies/RandomStrategy');

        it('Should trigger read events', function (done) {
            var idx = 0;
            var number_of_readings = 1;
            input_source.use(strategy({ "timeout_between_readings": 10 }));
            input_source
                .on('readable', function () {
                    var reading;
                    while ((reading = this.read()) !== null) {
                        if (++idx === number_of_readings) {
                            input_source.stop();
                            break;
                        }
                    }
                })
                .on('end', function () {
                    expect(idx).to.be.equal(number_of_readings);
                    done();
                })
                .start();
        });

    });
});
